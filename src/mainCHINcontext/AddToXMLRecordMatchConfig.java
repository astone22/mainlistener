package mainCHINcontext;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.postgresql.util.PSQLException;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ch.qos.logback.classic.Logger;

/**
 * 
 * @author Alan Stone
 *
 */



public class AddToXMLRecordMatchConfig {
	private static final Logger logger = (Logger) LoggerFactory.getLogger(AddToXMLRecordMatchConfig.class);
    public AddToXMLRecordMatchConfig(Connection conn, String sDir, List<String> newTablesAdd) {
    	List<String> newTables = newTablesAdd;
//    	System.out.println("AmtOfNewTables: " + newTables.size());
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
            Document document = docBuilder.parse(sDir);
        }
        catch(IOException | ParserConfigurationException | SAXException e) {
        	logger.info("EVENT: FILE NOT FOUND: " + sDir);
        }
            File fileScrubbed = new File(sDir);
    	    

		try{
        Connection connection = conn;
        Statement statement = null;
        Statement statement2 = null;
		statement = connection.createStatement();
		statement2 = connection.createStatement();
		
		
		
		  //
	      //	DB STUFF - get number of sources from db, and also add them to an arraylist
	      //
		for (int i = 0; i < newTablesAdd.size(); i++ ) {
	        ResultSet resultSet = null;
	        resultSet = statement.executeQuery("select global_datasource_id from chinmain_admin_datasources WHERE datasource_name = '" + newTables.get(i) + "';");
	        
	        while (resultSet.next()) {
	        	globalDatasourceID.add(resultSet.getInt("global_datasource_id"));
	        }
		
    		String line = "";
    		BufferedReader in2 = null;
    		
    		data[0] = " <dataset id=\"" + globalDatasourceID.get(i) + "\" name=\"" + newTables.get(i) + "\">";
            data[1] = "     <value>rec_match_input_" + globalDatasourceID.get(i) + ".txt</value>";
            data[2] = "     <dataset_index>";
            ResultSet resultSet2 = null;
            resultSet2 = statement2.executeQuery("select * from chinmain_admin_datasources_pi WHERE ds_id = '" + globalDatasourceID.get(i) + "';");
            resultSet2.next();
            int counter=0;
            String temp = "";
            for(int count=2; count<=11; count++){
            	if (resultSet2.getBoolean(count)){
                	temp+=counter + " ";
                	counter++;
                }
                else
                	temp+=-1 + " ";
            }
            temp = temp.substring(0, temp.length()-1);
            data[3] = "         <value>" + temp + "</value>";
    		data[4] = "     </dataset_index>";
    		data[5] = " </dataset>";
            String find = "<version-config-param id=\"1\">";
            
            
            
            
            
            
            
            
	    	    List<String> lines = new ArrayList<String>();
	    	    in2 = new BufferedReader(new FileReader(fileScrubbed));
	    	    line = in2.readLine();
	    	    
	    	    while (line != null) {
	    	    	if (line.contains(find)) {
	    	            line = "";
	    	            lines.add(data[0]);
	    	            lines.add(data[1]);
	    	            lines.add(data[2]);
	    	            lines.add(data[3]);
	    	            lines.add(data[4]);
	    	            lines.add(data[5]);
	    	            lines.add(find);
	    	            line = in2.readLine();
	    	        }
	    	    	else {
		    	        lines.add(line);
		    	        line = in2.readLine();
		    	        
	    	        }
	    	    }
	    	    
	    	    in2.close();
	    	    PrintWriter out;
				out = new PrintWriter(fileScrubbed);
	    	    for (String l : lines)
	    	        out.println(l);
	    	    out.close();
	    	    

	    	    
	    	    
	    	    System.out.println(newTables.get(i) + " data added to XML (" + sDir); 
	    	    logger.info("EVENT: Data added to XML file chin_main_record_match_config.xml");
    		}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (PSQLException e) {
			System.out.println("!!!!PSQL Exception on AddToXMLRecordMatchConfig.java!!!!");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("SQLException");
		}
		}
    
    private List<Integer> globalDatasourceID = new ArrayList();
    private String[] data = new String[15];
}

    		
            
    		
	    	    
	    	    
	    	    
	    	    
            
            
            
            
		



