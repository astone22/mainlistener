package mainCHINcontext;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

	import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import ch.qos.logback.classic.Logger;

public class BlockChanges {
	/**
	 * 
	 * @author Alan Stone
	 *
	 */
	private static final Logger logger = (Logger) LoggerFactory.getLogger(BlockChanges.class);
		public BlockChanges(Connection conn){
			try{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

	        File file2 = new File("C:\\java\\tomcat\\webapps\\chinapp\\WEB-INF\\files\\data_clustering\\config\\chin_main_record_match_config.xml");
	        String fileLocation2 = file2.getAbsolutePath();
	        Document document;
	        
	        try{
			document = docBuilder.parse(file2);
	        } catch(IOException e) {
	        	logger.info("EVENT: FILE NOT FOUND: chin_main_record_match_config.xml");
	        }
	        

	        Connection connection = conn;
			Statement statement = connection.createStatement();
			
			//
		    //	Update block index in XML file - OK
		    //
		    String index = "";
		    ResultSet resultSet = statement.executeQuery("select count(*) from chinmain_admin_cde_blockindex_view;");
		    resultSet.next();
		    int indexCount = resultSet.getInt(1);
//		    System.out.println("Index Count: " + indexCount);
			resultSet = statement.executeQuery("select block_index from chinmain_admin_cde_blockindex_view;");
			resultSet.next();
			for (int i = 0; i<indexCount; i++){
				index = index + resultSet.getInt("block_index");
				if (i != (indexCount -1))
					index = index + ",";
				resultSet.next();
			}
				

//		    System.out.println("Index: " + index);
		    String[] data = new String[15];
			String find = "<index>";
			data[0] = "            <value>" + index + "</value>";
			data[1] = "        <index>";
			String line = "";
			BufferedReader in2 = null;
			List<String> lines = new ArrayList<String>();
			try{
		    in2 = new BufferedReader(new FileReader(file2));
		    line = in2.readLine();
		    
		    while (line != null) {
		    	if (line.contains(find)) {
		    		line = "";
		            lines.add(data[1]);
		            lines.add(data[0]);
		            line = in2.readLine();
		            line = "";
		            line = in2.readLine();
		        }
		    	else {
	    	        lines.add(line);
	    	        line = in2.readLine();
		        }
		    }
		    in2.close();
			PrintWriter out = new PrintWriter(file2);
		    for (String l : lines)
		        out.println(l);
		    out.close();
		    
		    
		    
//		    //
//		    //	Update block length in XML file - NOT OK
//		    //
//		    String length = "";
//		    resultSet = statement.executeQuery("select count(*) from chinmain_admin_cde_blockdetails;");
//		    resultSet.next();
//		    int lengthCount = resultSet.getInt(1);
////		    System.out.println("Length Count: " + lengthCount);
//		    resultSet = statement.executeQuery("select count(block_length) from chinmain_admin_cde_blockdetails;");
//		    resultSet.next();
//		    int lengthOfBlock = resultSet.getInt(1);
////			System.out.println("Block Count: " + lengthOfBlock);
//		    resultSet = statement.executeQuery("select * from chinmain_admin_cde_details_new_view;");
//			resultSet.next();
//			int temp = 0;
//			int tempcdeid = 0;
//			for (int i = 0; i<lengthOfBlock; i++){
//				temp = resultSet.getInt("block_length");
//				if (temp != 0 ) {
//				tempcdeid = resultSet.getInt("cde_id");
//				System.out.println(tempcdeid);
//				if (tempcdeid != 0){
//					length = length + resultSet.getInt("cde_id");
//					if (i != (lengthOfBlock -1) && lengthOfBlock > 1)
//						length = length + ",";
//				}
//				}
//				resultSet.next();
//			}
		    String length = "";
		    resultSet = statement.executeQuery("select count(*) from chinmain_admin_cde_details_blocklength_view;");
		    resultSet.next();
		    int size = resultSet.getInt(1);
		    resultSet = statement.executeQuery("select * from chinmain_admin_cde_details_blocklength_view;");
		    resultSet.next();
		    int temp=0;
		    for (int i = 0; i<size; i++){
		    	length = length + resultSet.getInt("block_length");
					if (i != (size -1) && size > 1)
						length = length + ",";
				resultSet.next();
			}

//		    System.out.println("Length: " + length);
		    data = new String[15];
			find = "<length>";
			data[0] = "            <value>" + length + "</value>";
			data[1] = "        <length>";
			line = "";
			in2 = null;
			lines = new ArrayList<String>();
		    in2 = new BufferedReader(new FileReader(file2));
		    line = in2.readLine();
		    
		    while (line != null) {
		    	if (line.contains(find)) {
		    		line = "";
		            lines.add(data[1]);
		            lines.add(data[0]);
		            line = in2.readLine();
		            line = "";
		            line = in2.readLine();
		        }
		    	else {
	    	        lines.add(line);
	    	        line = in2.readLine();
		        }
		    }
		    in2.close();
			out = new PrintWriter(file2);
		    for (String l : lines)
		        out.println(l);
		    out.close();
		    
		    
		    
		    //
		    //	Update block type in XML file - OK
		    //
		    length = "";
		    resultSet = statement.executeQuery("select count(*) from chinmain_admin_cde_blockdetails;");
		    resultSet.next();
		    int lengthCount = resultSet.getInt(1);
//		    System.out.println("Type Count: " + lengthCount);
			resultSet = statement.executeQuery("select block_type from chinmain_admin_cde_blockdetails ORDER BY id;");
			resultSet.next();
			for (int i = 0; i<lengthCount; i++){
				length = length + resultSet.getInt("block_type");
				if (i != (lengthCount -1))
					length = length + ",";
				resultSet.next();
			}
				

		    System.out.println("Type: " + length);
		    data = new String[15];
			find = "<type>";
			data[0] = "            <value>" + length + "</value>";
			data[1] = "        <type>";
			line = "";
			in2 = null;
			lines = new ArrayList<String>();
		    in2 = new BufferedReader(new FileReader(file2));
		    line = in2.readLine();
		    
		    while (line != null) {
		    	if (line.contains(find)) {
		    		line = "";
		            lines.add(data[1]);
		            lines.add(data[0]);
		            line = in2.readLine();
		            line = "";
		            line = in2.readLine();
		        }
		    	else {
	    	        lines.add(line);
	    	        line = in2.readLine();
		        }
		    }
		    in2.close();
			out = new PrintWriter(file2);
		    for (String l : lines)
		        out.println(l);
		    out.close();
			}
			catch (IOException e) {
				logger.info("EVENT: FILE NOT FOUND: chin_main_record_match_config.xml");
			}
		    
		    System.out.println("Updated block info in chin_main_record_match_config.xml");
		    logger.info("EVENT: Data added to XML file chin_main_record_match_config.xml");
			}
			catch(ParserConfigurationException | SAXException | SQLException e) {
				e.printStackTrace();
			}
			
		}
	}
