package mainCHINcontext;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import ch.qos.logback.classic.Logger;

public class CDEDetailsUpdate {
	
	/**
	 * 
	 * @author Alan Stone
	 *
	 */
	private static final Logger logger = (Logger) LoggerFactory.getLogger(CDEDetailsUpdate.class);
	public CDEDetailsUpdate(Connection conn){
		try {
		logger.info("Starting CDE Details Update");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
        File file2 = new File("webapps/chinapp/WEB-INF/files/data_clustering/config/chin_main_record_match_config.xml");
        String fileLocation2 = file2.getAbsolutePath();
        
        Document document;
        try{
		document = docBuilder.parse(file2);
        } catch (IOException e) {
        	logger.info("EVENT: FILE NOT FOUND: chin_main_record_match_config.xml");
        }
        Connection connection = conn;
		Statement statement = connection.createStatement();
		

		
	
		
		
		
		//
		//	Write priority element in XML file (insert cde_id in order of priority)
		//
		ResultSet resultSet = statement.executeQuery("select count(*) from chinmain_admin_cde_details_view");
		resultSet.next();
		int priorityCount = resultSet.getInt(1);
//		System.out.println("Priority Count = " + priorityCount);
		resultSet = statement.executeQuery("select cde_id from chinmain_admin_cde_details_view;");
		String result = "";
		resultSet.next();
		for (int i = 0; i<priorityCount; i++ ){
			result = result + resultSet.getInt("cde_id");
			if (i != (priorityCount -1))
				result = result + ",";
			resultSet.next();
		}
//		System.out.println("Priorities: " + result);
		String[] data = new String[15];
		String find = "<priority>";
		data[0] = "            <value>" + result + "</value>";
		data[1] = "        <priority>";
		String line = "";
		BufferedReader in2 = null;
		List<String> lines = new ArrayList<String>();
		
		
		try{
	    in2 = new BufferedReader(new FileReader(file2));
	    line = in2.readLine();
	    
	    while (line != null) {
	    	if (line.contains(find)) {
	    		line = "";
	            lines.add(data[1]);
	            lines.add(data[0]);
	            line = in2.readLine();
	            line = "";
	            line = in2.readLine();
	        }
	    	else {
    	        lines.add(line);
    	        line = in2.readLine();
	        }
	    }
	    in2.close();
	    PrintWriter out;
		out = new PrintWriter(file2);
	    for (String l : lines)
	        out.println(l);
	    out.close();
	    
	    
	    
	    
	    

		//
		//  Edit Comparison method
		//

		int[] cdeid = new int[8];
		int[] cdecomparisonmethod = new int[8];
		int[] cdetruncdistance = new int[8];
		//String query = "SELECT * FROM \"chinmain_admin_cde_details\" WHERE \"chinmain_admin_cde_details\".\"cde_id\" != 1;";
		String query = "SELECT * FROM chinmain_admin_cde_details;";
		ResultSet rs = statement.executeQuery(query);
		rs.next();
		int cdetemp = 0;
		while(rs.next()){
			cdeid[cdetemp] = rs.getInt("cde_id") - 1;
			cdecomparisonmethod[cdetemp] = rs.getInt("matching_process");
			cdetruncdistance[cdetemp] = rs.getInt("truncation_distance");
			cdetemp++;
		}
		
		String[] cdenames = new String[8];
		for (int i = 0; i < 8; i++){
			if (i==0) cdenames[i] = "first";
			if (i==1) cdenames[i] = "last";
			if (i==2) cdenames[i] = "ssn";
			if (i==3) cdenames[i] = "dob";
			if (i==4) cdenames[i] = "sasid";
			if (i==5) cdenames[i] = "hscode";
			if (i==6) cdenames[i] = "mname";
			if (i==7) cdenames[i] = "gender";
			
			find = "<comparison id=\"" + cdenames[i] + "\">";
			
			line = "";
			in2 = null;
			lines = new ArrayList<String>();
		    in2 = new BufferedReader(new FileReader(file2));
		    line = in2.readLine();
		    
		    while (line != null) {
		    	if (line.contains(find)) {
		    		line = find;
		            lines.add("        " + find);
		            line = in2.readLine();
		            lines.add(line); //dist_calc_method
		            line = in2.readLine();
		            line = "                <value>" + cdecomparisonmethod[i] + "</value>"; 
		            lines.add(line); //value
		            line = in2.readLine();
		            lines.add(line); // end dist_calc_method
		            line = in2.readLine();
		            lines.add(line);
		            line = in2.readLine();
		            lines.add(line);
		            line = in2.readLine();
		            lines.add(line);
		            
		            if (cdecomparisonmethod[i] == 3){
		            	lines.add("            <truncate_count>");
		            	lines.add("                <value>" + cdetruncdistance[i] + "</value>");
		            	lines.add("            </truncate_count>");
		            	line = in2.readLine();
		            	line = in2.readLine();
		            	line = in2.readLine();
		            	line = in2.readLine();
		            	line = in2.readLine();
		            }
		            if (cdecomparisonmethod[i] == 1 || cdecomparisonmethod[i] == 0){
		            	lines.add("");
		            	lines.add("");
		            	lines.add("");
		            	line = in2.readLine();
		            	line = in2.readLine();
		            	line = in2.readLine();
		            	line = in2.readLine();
		            	line = in2.readLine();
		            }
		            lines.add("        </comparison>");
		        }
		    	else {
	    	        lines.add(line);
	    	        line = in2.readLine();
		        }
		    }
		    in2.close();
			out = new PrintWriter(file2);
		    for (String l : lines)
		        out.println(l);
		    out.close();
		    
		    
		}


	    //
	    //	Update weight in XML file - Version 2 (insert zeros) -  (insert weights)
	    //
	    String weight = "";
		resultSet = statement.executeQuery("select * from chinmain_admin_cde_details ORDER BY cde_id;");
		while(resultSet.next()){
			if (weight.equalsIgnoreCase("")){
				weight = weight + resultSet.getInt("weights");
			}
			else{
				weight = weight + " " + resultSet.getInt("weights");
			}
		}
			

	    //System.out.println("Weights: " + weight);
	    
		find = "<weights>";
		data[0] = "            <value>" + weight + "</value>";
		data[1] = "        <weights>";
		line = "";
		in2 = null;
		lines = new ArrayList<String>();
	    in2 = new BufferedReader(new FileReader(file2));
	    line = in2.readLine();
	    
	    while (line != null) {
	    	if (line.contains(find)) {
	    		line = "";
	            lines.add(data[1]);
	            lines.add(data[0]);
	            line = in2.readLine();
	            line = "";
	            line = in2.readLine();
	        }
	    	else {
    	        lines.add(line);
    	        line = in2.readLine();
	        }
	    }
	    in2.close();
		out = new PrintWriter(file2);
	    for (String l : lines)
	        out.println(l);
	    out.close();
	    
		}
		catch (IOException e) {
			logger.info("EVENT: FILE NOT FOUND: chin_main_record_match_config.xml");
		}
		
		
		
//	    //
//	    //	Update weight in XML file - Version 1 (hide zeros) -  (insert weights)
//	    //
//	    String weight = "";
//	    resultSet = statement.executeQuery("select count(*) from chinmain_admin_cde_details_new_view;");
//	    resultSet.next();
//	    int weightCount = resultSet.getInt(1);
////	    System.out.println("Weight Count: " + weightCount);
//		resultSet = statement.executeQuery("select weights from chinmain_admin_cde_details_new_view ORDER BY cde_id;");
//		resultSet.next();
//		for (int i = 0; i<weightCount; i++){
//			weight = weight + resultSet.getInt("weights");
//			if (i != (weightCount -1))
//				weight = weight + " ";
//			resultSet.next();
//		}
//			
//
////	    System.out.println("Weights: " + weight);
//	    
//		find = "<weights>";
//		data[0] = "            <value>" + weight + "</value>";
//		data[1] = "        <weights>";
//		line = "";
//		in2 = null;
//		lines = new ArrayList<String>();
//	    in2 = new BufferedReader(new FileReader(file2));
//	    line = in2.readLine();
//	    
//	    while (line != null) {
//	    	if (line.contains(find)) {
//	    		line = "";
//	            lines.add(data[1]);
//	            lines.add(data[0]);
//	            line = in2.readLine();
//	            line = "";
//	            line = in2.readLine();
//	        }
//	    	else {
//    	        lines.add(line);
//    	        line = in2.readLine();
//	        }
//	    }
//	    in2.close();
//		out = new PrintWriter(file2);
//	    for (String l : lines)
//	        out.println(l);
//	    out.close();
//	    
//		}
//		catch (IOException e) {
//			logger.info("EVENT: FILE NOT FOUND: chin_main_record_match_config.xml");
//		}
	    
	    

		//
	    //	Update block index in XML file   (insert cde_id's that have an entry in block_index and in order of block_index's entries)
	    //
	    String index = "";
	    resultSet = statement.executeQuery("select count(*) from chinmain_admin_cde_blockindex_view;");
	    resultSet.next();
	    int indexCount = resultSet.getInt(1);
//	    System.out.println("Index Count: " + indexCount);
		resultSet = statement.executeQuery("select cde_id from chinmain_admin_cde_blockindex_view;");
		resultSet.next();
		for (int i = 0; i<indexCount; i++){
			index = index + resultSet.getInt("cde_id");
			if (i != (indexCount -1))
				index = index + ",";
			resultSet.next();
		}
			

//	    System.out.println("Index: " + index);
	    data = new String[15];
		find = "<index>";
		data[0] = "            <value>" + index + "</value>";
		data[1] = "        <index>";
		line = "";
		in2 = null;
		lines = new ArrayList<String>();
		try{
	    in2 = new BufferedReader(new FileReader(file2));
	    line = in2.readLine();
	    
	    while (line != null) {
	    	if (line.contains(find)) {
	    		line = "";
	            lines.add(data[1]);
	            lines.add(data[0]);
	            line = in2.readLine();
	            line = "";
	            line = in2.readLine();
	        }
	    	else {
    	        lines.add(line);
    	        line = in2.readLine();
	        }
	    }
	    in2.close();
		PrintWriter out = new PrintWriter(file2);
	    for (String l : lines)
	        out.println(l);
	    out.close();
	    
	    
	    
	    //
	    //	Update block length in XML file  (enter block_length as-is)
	    //
	    String length = "";
	    resultSet = statement.executeQuery("select count(*) from chinmain_admin_cde_details_blocklength_view where chinmain_admin_cde_details_blocklength_view.block_length IS NOT NULL;");
	    resultSet.next();
	    int lengthCount = resultSet.getInt(1);
//	    System.out.println("Length Count: " + lengthCount);
	    resultSet = statement.executeQuery("select block_length from chinmain_admin_cde_details_blocklength_view where chinmain_admin_cde_details_blocklength_view.block_length IS NOT NULL;");
	    resultSet.next();
	    for (int i = 0; i<lengthCount; i++){
	    	length = length + resultSet.getInt("block_length");
				if (i != (lengthCount -1) && lengthCount > 1)
					length = length + ",";
			resultSet.next();
		}
			

//	    System.out.println("Length: " + length);
	    data = new String[15];
		find = "<length>";
		data[0] = "            <value>" + length + "</value>";
		data[1] = "        <length>";
		line = "";
		in2 = null;
		lines = new ArrayList<String>();
	    in2 = new BufferedReader(new FileReader(file2));
	    line = in2.readLine();
	    
	    while (line != null) {
	    	if (line.contains(find)) {
	    		line = "";
	            lines.add(data[1]);
	            lines.add(data[0]);
	            line = in2.readLine();
	            line = "";
	            line = in2.readLine();
	        }
	    	else {
    	        lines.add(line);
    	        line = in2.readLine();
	        }
	    }
	    in2.close();
		out = new PrintWriter(file2);
	    for (String l : lines)
	        out.println(l);
	    out.close();
	    
	    
	    
	    //
	    //	Update block type in XML file   (enter block_type as-is)
	    //
	    length = "";
	    resultSet = statement.executeQuery("select count(*) from chinmain_admin_cde_details_new_view WHERE chinmain_admin_cde_details_new_view.block_type IS NOT NULL;");
	    resultSet.next();
	    lengthCount = resultSet.getInt(1);
//	    System.out.println("Type Count: " + lengthCount);
		resultSet = statement.executeQuery("select block_type from chinmain_admin_cde_details_new_view WHERE chinmain_admin_cde_details_new_view.block_type IS NOT NULL ORDER BY cde_id;");
		resultSet.next();
		for (int i = 0; i<lengthCount; i++){
			length = length + resultSet.getInt("block_type");
			if (i != (lengthCount -1))
				length = length + ",";
			resultSet.next();
		}
			

//	    System.out.println("Type: " + length);
	    data = new String[15];
		find = "<type>";
		data[0] = "            <value>" + length + "</value>";
		data[1] = "        <type>";
		line = "";
		in2 = null;
		lines = new ArrayList<String>();
	    in2 = new BufferedReader(new FileReader(file2));
	    line = in2.readLine();
	    
	    while (line != null) {
	    	if (line.contains(find)) {
	    		line = "";
	            lines.add(data[1]);
	            lines.add(data[0]);
	            line = in2.readLine();
	            line = "";
	            line = in2.readLine();
	        }
	    	else {
    	        lines.add(line);
    	        line = in2.readLine();
	        }
	    }
	    in2.close();
		out = new PrintWriter(file2);
	    for (String l : lines)
	        out.println(l);
	    out.close();
		}
		catch (IOException e) {
			logger.info("EVENT: FILE NOT FOUND: chin_main_record_match_config.xml");
		}
		
		System.out.println("CDE/Block update to chin_main_record_match_config.xml");
		logger.info("EVENT: Data added to XML file chin_main_record_match_config.xml");
        
		}
		catch(ParserConfigurationException | SAXException | SQLException e) {
			e.printStackTrace();
		}
		
	}
}
