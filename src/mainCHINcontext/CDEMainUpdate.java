package mainCHINcontext;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import ch.qos.logback.classic.Logger;

public class CDEMainUpdate {
	private static final Logger logger = (Logger) LoggerFactory.getLogger(CDEMainUpdate.class);
	public CDEMainUpdate(Connection conn){
		try {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
        File file2 = new File("webapps/chinapp/WEB-INF/files/data_clustering/config/chin_main_record_match_config.xml");
        String fileLocation2 = file2.getAbsolutePath();
        //logger.info("DEBUG: looking for xml in: " + fileLocation2);
        
        Document document;
        try{
		document = docBuilder.parse(file2);
        } catch (IOException e) {
        	logger.info("EVENT: FILE NOT FOUND: chin_main_record_match_config.xml");
        }
		

        Connection connection = conn;
		Statement statement = connection.createStatement();
		Statement statement2 = connection.createStatement();
		
		//
		//  Update weights in chinmain_admin_cde_details to zero if chinmain_admin_cde is_used_for_matching? set to false
		//
		Boolean[] cdebool = new Boolean[9];
		int[] cdeint = new int[9];

		ResultSet rs = statement.executeQuery("select * from chinmain_admin_cde");
		int cdetemp = 0;
		while (rs.next()){
			cdebool[cdetemp] = rs.getBoolean("is_used_for_matching");
//			System.out.println(cdebool[cdetemp]);
			if (!cdebool[cdetemp]){
				String a = "UPDATE chinmain_admin_cde_details SET weights = 0 WHERE cde_id = " + (cdetemp + 1) + ";";
//				System.out.println(a);
				logger.info("EVENT: " + a);
				statement2.executeUpdate(a);
			}
			cdetemp++;
		}
		
		
		
		}
		catch(ParserConfigurationException | SAXException | SQLException e) {
			e.printStackTrace();
		}
		
	}
}
