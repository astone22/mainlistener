package mainCHINcontext;


import java.io.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.sql.*;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;

import ch.qos.logback.classic.Logger;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;









import java.util.ArrayList;









import javax.xml.parsers.*;
import javax.xml.xpath.*;

import java.io.*;

import org.postgresql.util.PSQLException;
import org.slf4j.LoggerFactory;
import org.w3c.dom.*;
import org.xml.sax.*;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * 
 * @author Alan Stone
 *
 */
public class syncXML {
	private static final Logger logger = (Logger) LoggerFactory.getLogger(syncXML.class);
    public syncXML(Connection conn){
    	try {
    	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
        
        Connection connection = conn;
		Statement statement = connection.createStatement();
		
		  //
	      //	DB STUFF - get number of sources from db, and add them to an arraylist
	      //
	        ResultSet resultSet = null;
	        resultSet = statement.executeQuery("select count(datasource_name) from chinmain_admin_datasources;");
	        resultSet.next();
	        int numberOfSourcesFromDB = resultSet.getInt(1);
	        
	        
	        List<String> DBList = new ArrayList<>(numberOfSourcesFromDB);
			resultSet = statement.executeQuery("select * from chinmain_admin_datasources;");
			while (resultSet.next()){
		        String datasourceName = resultSet.getString("datasource_name");
//		        System.out.println("Datasource name from DB is: " + datasourceName);
		      	DBList.add(datasourceName);
	        }
			System.out.println("Number of sources from DB: " + numberOfSourcesFromDB);
		
		//
		//	XML STUFF
		//
			// -opens xml file into document
        File file = new File("webapps/chinapp/WEB-INF/mainCHINContext.xml");
        String fileLocation = file.getAbsolutePath();
        logger.info("DEBUG: #1 looking for xml in: " + fileLocation);
        Document document = docBuilder.parse(file);
        
        File file2 = new File("webapps/chinapp/WEB-INF/files/data_clustering/config/chin_main_record_match_config.xml");
        String fileLocation2 = file2.getAbsolutePath();
        logger.info("DEBUG: #2 looking for xml in: " + fileLocation2);

        XPathFactory xpFactory = XPathFactory.newInstance();
        XPath xPath = xpFactory.newXPath();
        document.getDocumentElement().normalize();
//        System.out.println("Root element: " + document.getDocumentElement().getNodeName());

        XPathExpression expr = xPath.compile("//beans/bean"); 
        NodeList  nList = (NodeList) expr.evaluate(document, XPathConstants.NODESET); //

        
        //
        //	Grab the number of sources from XML also add them to an ArrayList
        //
        //
        String xmlID = "";
        String xmlID2 = "";
        String xmlIDsource = "";
        int numberOfSourcesFromXML = 0;
        String beanID1 = "testRmtServiceHttpInvokerProxy";
        String beanID2 = "chinRmtAppInitializeServiceInterfaceRef";
        String beanID3 = "CHINRemoteAppInitializeClient";
        String beanID4 = "MetadataServiceInterface";
        
        List<String> XMLList = new ArrayList<>();
        for (int temp = 0; temp < nList.getLength(); temp++) {
        	
            Node nNode = nList.item(temp);
            xmlID = ((Element) nNode).getAttribute("id");
//            System.out.println(xmlID);
            
            try{
            	xmlID2 = xmlID.substring(0, 30);
            	xmlIDsource = xmlID.substring(30, xmlID.length());
            }
            catch(StringIndexOutOfBoundsException e){
            	//e.printStackTrace();
            }

            if (xmlID2.equalsIgnoreCase(beanID1)){
            	//System.out.println("match");
            	numberOfSourcesFromXML++;
            	XMLList.add(xmlIDsource);
//            	System.out.println("source from xml is: " + xmlIDsource);
            }
        }

        System.out.println("Number of sources from XML: " + numberOfSourcesFromXML);
	        
        //
        //	
        //	COMPARE DB AND XML ENTRIES - CHECK FOR DELETION FROM DB - IF DELETED FROM DB, DELETE FROM XML
        //
        
        if (numberOfSourcesFromXML > numberOfSourcesFromDB) {
	      
	        boolean matchFound = false;
	    	int countTemp = 0;
	    	System.out.println("XML List: " + XMLList);
	    	System.out.println("DB List: " + DBList);
	    	List<String> newTables = new ArrayList<>();
	    	for (String i : XMLList) {
	    		for (String j : DBList) {
	    			if (i.equalsIgnoreCase(j)) {
	    				matchFound = true;
	    				break;
	    						}
	    					}
	    		if (!matchFound) {
	    			newTables.add(i);
	    			countTemp++;
	    						}
	    		matchFound = false;
	    	}
	        
	    	if (newTables.isEmpty()){
	    		System.out.println("No data sources have been deleted from the DB.");
	    	}
	    	else {
//	    		System.out.println("----Deleting Data Sources From XML----");
	    		for ( String ds : newTables ) {
		    		RemoveFromXML rfxml = new RemoveFromXML(fileLocation, ds);
		    		RemoveFromXMLRecordMatchConfig rfrecordXML = new RemoveFromXMLRecordMatchConfig(fileLocation2, ds);
//		    		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//					Date date = new Date();
//					Statement stmt = connection.createStatement();
//					try {
//					statement.executeQuery("INSERT INTO alan_logging_test(short_name, event, listener, time) VALUES (\'" + newTables.get(countTemp - 1) + "\', \'DELETE: Row(s) in chinmain_admin_datasources have been deleted.\', \'dsdelete\', \'" + dateFormat.format(date) + "\' )");
//					}
//					catch(PSQLException e){
//						//e.printStackTrace();
//					}
//					System.out.println("***INSERTED DATA REGARDING DELETE TO LOG TABLE***");
//					statement.close();
		    	}
	    		System.out.println("Deleted Data Source List: " + newTables);
	    	}
        }
	    	
        
        //
        //	
        //	COMPARE DB AND XML ENTRIES - CHECK FOR ADDITION TO DB - IF ADDED TO DB, ADD TO XML
        //
	    
	    else if (numberOfSourcesFromDB > numberOfSourcesFromXML ) {
	    	boolean matchFoundAdd = false;
	    	int countTempAdd = 0;
//	    	System.out.println("XML List: " + XMLList);
//	    	System.out.println("DB List: " + DBList);
	    	List<String> newTablesAdd = new ArrayList<>();
	    	for (String i : DBList) {
	    		for (String j : XMLList) {
	    			if (i.equalsIgnoreCase(j)) {
	    				matchFoundAdd = true;
	    				break;
	    						}
	    					}
	    		if (!matchFoundAdd) {
	    			newTablesAdd.add(i);
	    			countTempAdd++;
	    						}
	    		matchFoundAdd = false;
	    	}
	    	if (newTablesAdd.isEmpty()){
	    		System.out.println("No new sources have been added to the DB.");
	    		System.exit(0);
	    	}
	    	else {
	    		System.out.println("Newly Added Source List: " + newTablesAdd);
	    		AddToXML plop = new AddToXML(connection, fileLocation, newTablesAdd);
	    		
	    		
// UNCOMMENT FOR ONYIS GETCDE TO WORK	    		
	    		GetCDE getcde = new GetCDE();
	    		for (String test : newTablesAdd){
	    			getcde.readCDE(test, connection);
//	    			Runtime re = Runtime.getRuntime();	
//	    			re.exec("java -jar C:\\java\\tomcat\\webapps\\Pathm\\WEB-INF\\files\\getCDE.jar " + test); 
	    		}
	    		
	    		AddToXMLRecordMatchConfig recordXML = new AddToXMLRecordMatchConfig(connection, fileLocation2, newTablesAdd);
	    		
//	    		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//				Date date = new Date();
//				Statement stmt = connection.createStatement();
//				try {
//				statement.executeQuery("INSERT INTO alan_logging_test(short_name, event, listener, time) VALUES (\'" + newTablesAdd.get(countTempAdd - 1) + "\', \'INSERT: Row(s) have been inserted to chinmain_admin_datasources.\', \'dsinsert\', \'" + dateFormat.format(date) + "\' )");
//				}
//				catch(PSQLException e){
//					//e.printStackTrace();
//				}
//				System.out.println("***INSERTED DATA REGARDING INSERT TO LOG TABLE***");
//				statement.close();
	    	}
	    }
    	
    	

		//
        //
        //	ERROR CATCHING
        //
    } catch (SQLException e) {
        System.out.println("SQLException");
        e.printStackTrace();
    } catch (XPathExpressionException e) {
        System.out.println("XPathExpressionException");
        e.printStackTrace();
    } catch (IOException e) {
        logger.info("EVENT: FILE NOT FOUND");
    } catch (ParserConfigurationException e) {
        System.out.println("ParserConfigurationException");
        e.printStackTrace();
    } catch (SAXException e) {
        System.out.println("SAXException");
        e.printStackTrace();
    }
	}
}
