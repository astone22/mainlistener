package mainCHINcontext;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import ch.qos.logback.classic.Logger;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class UpdateThreshold {
	
	/**
	 * 
	 * @author Alan Stone
	 *
	 */
	private static final Logger logger = (Logger) LoggerFactory.getLogger(UpdateThreshold.class);
		public UpdateThreshold(Connection conn){
		try {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
        File file2 = new File("webapps/chinapp/WEB-INF/files/data_clustering/config/chin_main_record_match_config.xml");
        String fileLocation2 = file2.getAbsolutePath();
        logger.info("DEBUG: looking for xml in: " + fileLocation2);
        
        
		Document document = docBuilder.parse(file2);

        Connection connection = conn;
		Statement statement = connection.createStatement();
		
		ResultSet resultSet = statement.executeQuery("select threshold from chinmain_admin_cde_threshold;");
		resultSet.next();
		String threshold = resultSet.getString("threshold");

		String[] data = new String[15];
		String find = "<threshold>";
		String find2 = "	     <threshold>";
		data[0] = "	        <value>" + threshold + "</value>";
		
		String line = "";
		BufferedReader in2 = null;
		List<String> lines = new ArrayList<String>();
	    in2 = new BufferedReader(new FileReader(file2));
	    line = in2.readLine();
	    
	    while (line != null) {
	    	if (line.contains(find)) {
	            line = "";
	            lines.add(find2);
	            lines.add(data[0]);
	            line = in2.readLine();
	            line = "";
	            line = in2.readLine();
	        }
	    	else {
    	        lines.add(line);
    	        line = in2.readLine();
	        }
	    }
	    in2.close();
	    PrintWriter out;
		out = new PrintWriter(file2);
	    for (String l : lines)
	        out.println(l);
	    out.close();

		System.out.println("Updated threshold element in chin_main_record_match_config.xml");
		logger.info("EVENT: Data updated in chin_main_record_match_config.xml");
        
		}
		catch(ParserConfigurationException | SAXException | SQLException e) {
			e.printStackTrace();
		}
		catch(IOException e){
			logger.info("EVENT: FILE NOT FOUND: chin_main_record_match_config.xml");
		}
	}
}
