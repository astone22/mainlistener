package mainCHINcontext;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.slf4j.*;

import ch.qos.logback.classic.Logger;

/**
 * 
 * @author Alan Stone
 *
 */

public class AddToXML {
	private static final Logger logger = (Logger) LoggerFactory.getLogger(AddToXML.class);
    public AddToXML(Connection conn, String sDir, List<String> newTablesAdd) {
    	List<String> newTables = newTablesAdd;
//    	System.out.println("AmtOfNewTables: " + newTables.size());
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
            Document document = docBuilder.parse(sDir);
        }
        catch(IOException | ParserConfigurationException | SAXException e) {
        	logger.info("EVENT: FILE NOT FOUND: " + sDir);
        }
            File fileScrubbed = new File(sDir);
    	    
    	
		try {
        Connection connection = conn;
		Statement statement = connection.createStatement();
		
		
		
		  //
	      //	DB STUFF - get number of sources from db, and also add them to an arraylist
	      //
		for (int i = 0; i < newTablesAdd.size(); i++ ) {
	        ResultSet resultSet = null;
	        resultSet = statement.executeQuery("select remote_instance_ip_addr from chinmain_admin_datasources WHERE datasource_name = '" + newTables.get(i) + "';");
	        
	        while (resultSet.next()) {
	        	ipAddress.add(resultSet.getString("remote_instance_ip_addr"));
	        }
		
    		String line = "";
    		BufferedReader in2 = null;
    		
    		data[0] = "  <bean class=\"org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean\" id=\"testRmtServiceHttpInvokerProxy" + newTables.get(i) + "\">";
            data[1] = "   <property name=\"serviceUrl\" value=\"" + ipAddress.get(i) + "/chinrmtmvcsvc/testRmtService-httpinvoker\"/>";
            data[2] = "   <property name=\"serviceInterface\" value=\"com.akazaresearch.phenonet.server.chinrmt.springremote.interf.TestRmtServiceInterface\"/>";
            data[3] = "   <property name=\"httpInvokerRequestExecutor\">";
    		data[4] = "    <bean class=\"org.springframework.security.context.httpinvoker.AuthenticationSimpleHttpInvokerRequestExecutor\"/>";
    		data[5] = "   </property>";
    		data[6] = " </bean>";
            String find = "</beans>";
            String space = "";
            data[7] = " <bean class=\"org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean\" id=\"chinRmtAppInitializeServiceInterfaceRef" + newTables.get(i) + "\">";
            data[8] = "   <property name=\"serviceUrl\" value=\"" + ipAddress.get(i) + "/chinrmtmvcsvc/chinRmtAppInit-httpinvoker\"/>";
    		data[9] = "   <property name=\"serviceInterface\" value=\"com.akazaresearch.phenonet.server.main.init.ChinRmtAppInitializeServiceInterface\"/>";
            data[10] = " <bean class=\"com.akazaresearch.phenonet.server.main.init.CHINRemoteAppInitializeClient\" id=\"CHINRemoteAppInitializeClient" + newTables.get(i) + "\">";
            data[11] = "   <property name=\"chinRmtAppInitializeServiceInterface\" ref=\"chinRmtAppInitializeServiceInterfaceRef" + newTables.get(i) + "\"/>";
            data[12] = " <bean class=\"org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean\" id=\"MetadataServiceInterface" + newTables.get(i) + "\">";
            data[13] = "   <property name=\"serviceUrl\" value=\"" + ipAddress.get(i) + "/chinrmtmvcsvc/QRMetadata-httpinvoker\"/>";
            data[14] = "   <property name=\"serviceInterface\" value=\"com.akazaresearch.phenonet.server.chinrmt.springremote.interf.QRMetadataServiceInterface\"/>";
            
            
    		int counter = 0;
//    			System.out.println("Adding: " + newTablesAdd.get(i));
	    	    List<String> lines = new ArrayList<String>();
	    	    in2 = new BufferedReader(new FileReader(fileScrubbed));
	    	    line = in2.readLine();
	    	    
	    	    while (line != null) {
	    	    	if (line.contains(find)) {
	    	            line = "";
	    	            lines.add(data[0]);
	    	            lines.add(data[1]);
	    	            lines.add(data[2]);
	    	            lines.add(data[3]);
	    	            lines.add(data[4]);
	    	            lines.add(data[5]);
	    	            lines.add(data[6]);
	    	            lines.add(space);
	    	            lines.add(data[7]);
	    	            lines.add(data[8]);
	    	            lines.add(data[9]);
	    	            lines.add(data[3]);
	    	            lines.add(data[4]);
	    	            lines.add(data[5]);
	    	            lines.add(data[6]);
	    	            lines.add(space);
	    	            lines.add(data[10]);
	    	            lines.add(data[11]);
	    	            lines.add(data[6]);
	    	            lines.add(space);
	    	            lines.add(data[12]);
	    	            lines.add(data[13]);
	    	            lines.add(data[14]);
	    	            lines.add(data[3]);
	    	            lines.add(data[4]);
	    	            lines.add(data[5]);
	    	            lines.add(data[6]);
	    	            lines.add(space);
	    	            lines.add(find);
	    	            line = in2.readLine();
	    	        }
	    	    	else {
		    	        lines.add(line);
		    	        line = in2.readLine();
		    	        
	    	        }
	    	    }
	    	    
	    	    in2.close();
	    	    PrintWriter out;
				out = new PrintWriter(fileScrubbed);
	    	    for (String l : lines)
	    	        out.println(l);
	    	    out.close();
	    	    System.out.println(newTables.get(i) + " data added to XML (" + sDir); 
	    	    logger.info("EVENT: Data added to XML file mainCHINcontext.xml");
    		
    		}
		}
		catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}
    


	private List<String> ipAddress = new ArrayList();
    private String[] data = new String[15];
}

    		
            
    		
	    	    
	    	    
	    	    
	    	    
            
            
            
            
		



