package mainCHINcontext;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ch.qos.logback.classic.Logger;

/**
 *
 * @author Dzmitry Siaukovich
 */
public class RemoveFromXMLRecordMatchConfig {
	private static final Logger logger = (Logger) LoggerFactory.getLogger(RemoveFromXMLRecordMatchConfig.class);
    public RemoveFromXMLRecordMatchConfig(String sDir, String sCompanyAbbreviation) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
            Document document = docBuilder.parse(sDir);
            

    	    
            

            
            XPath xpath = XPathFactory.newInstance().newXPath();
            
            
//            XPathExpression expr = xpath.compile("//bean[@id=\"testRmtServiceHttpInvokerProxy" + sCompanyAbbreviation + "\"]");
//            Node n = ((NodeList) expr.evaluate(document, XPathConstants.NODESET)).item(0);
//            while (n.hasChildNodes()) {
//                n.removeChild(n.getFirstChild());
//            }
//            n.getParentNode().removeChild(n);
//            
            XPathExpression expr = xpath.compile("//dataset[@name=\"" + sCompanyAbbreviation + "\"]");
            Node n = ((NodeList) expr.evaluate(document, XPathConstants.NODESET)).item(0);
            while (n.hasChildNodes()) {
                n.removeChild(n.getFirstChild());
            }
            n.getParentNode().removeChild(n);
            
//            
//            
//            
//
//            expr = xpath.compile("//bean[@id=\"chinRmtAppInitializeServiceInterfaceRef" + sCompanyAbbreviation + "\"]");
//            n = ((NodeList) expr.evaluate(document, XPathConstants.NODESET)).item(0);
//            while (n.hasChildNodes()) {
//                n.removeChild(n.getFirstChild());
//            }
//            n.getParentNode().removeChild(n);
//            expr = xpath.compile("//bean[@id=\"CHINRemoteAppInitializeClient" + sCompanyAbbreviation + "\"]");
//            n = ((NodeList) expr.evaluate(document, XPathConstants.NODESET)).item(0);
//            while (n.hasChildNodes()) {
//                n.removeChild(n.getFirstChild());
//            }
//            n.getParentNode().removeChild(n);
//            expr = xpath.compile("//bean[@id=\"MetadataServiceInterface" + sCompanyAbbreviation + "\"]");
//            n = ((NodeList) expr.evaluate(document, XPathConstants.NODESET)).item(0);
//            while (n.hasChildNodes()) {
//                n.removeChild(n.getFirstChild());
//            }
//            n.getParentNode().removeChild(n);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            document.setXmlStandalone(false);
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(sDir);
            transformer.transform(source, result);
            System.out.println("Removed data source(s) from chin_main_record_match_config.xml");
            logger.info("EVENT: Data removed from chin_main_record_match_config.xml");
            

        } catch (ParserConfigurationException e) {
            System.out.println("ParserConfigurationException");
            e.printStackTrace();
            System.exit(1);
        } catch (SAXException e) {
            System.out.println("SAXException");
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            logger.info("EVENT: FILE NOT FOUND: chin_main_record_match_config.xml");
        } catch (TransformerConfigurationException e) {
            System.out.println("TransformerConfigurationException");
            e.printStackTrace();
            System.exit(1);
        } catch (TransformerException e) {
            System.out.println("TransformerException");
            e.printStackTrace();
            System.exit(1);
        } catch (XPathExpressionException e) {
            System.out.println("XPathExpressionException");
            e.printStackTrace();
            System.exit(1);
        } catch (NullPointerException e) {
        	System.out.println("!!!!Null Pointer Exception on RemoveFromXMLRecordMatchConfig.java!!!!");
        	e.printStackTrace();
        }
        
    }
}
