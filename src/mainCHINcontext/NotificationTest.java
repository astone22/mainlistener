package mainCHINcontext;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Date;
import java.util.Properties;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.postgresql.util.PSQLException;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

public class NotificationTest {
	/**
	 * 
	 * @author Alan Stone
	 *
	 */
	private static final Logger logger = (Logger) LoggerFactory.getLogger(NotificationTest.class);
	
	public static void main(String args[]) throws Exception {
        //
        //	Get the connection info from data.properties for connecting to chin_main
        //
		
		logger.info("EVENT: Main listener started!");
		
        Properties prop = new Properties();
		String location = null;
		String userName = null;
		String password = null;
		try {
            prop.load(new FileInputStream("webapps/Pathm/WEB-INF/files/data.properties"));
			location = prop.getProperty("location");
            userName = prop.getProperty("user");
            password = prop.getProperty("password");
        } catch (IOException ex) {
            System.out.println("File data.properties not found");
            ex.printStackTrace();
            System.exit(1);
        }
		//
        //	make connection to chin_main (location, userName, password - are from data.properties)
		//
        Connection conn = DriverManager.getConnection(location, userName, password);

		//Class.forName("org.postgresql.Driver");

		// Create two distinct connections, one for the notifier
		// and another for the listener to show the communication
		// works across connections although this example would
		// work fine with just one connection.
        
		// Create two threads, one to issue notifications and
		// the other to receive them.
		Listener listener = new Listener(conn);
		//Notifier notifier = new Notifier(nConn);
		listener.start();
		System.out.println("***Listening to: " + location + "***");
		System.out.println("***Press Ctrl+C to exit***");
		//notifier.start();
	}

}

class Listener extends Thread {
	private static final Logger logger = (Logger) LoggerFactory.getLogger(NotificationTest.class);
	private Connection conn;
	private org.postgresql.PGConnection pgconn;

	Listener(Connection conn) throws SQLException {
		this.conn = conn;
		this.pgconn = (org.postgresql.PGConnection)conn;
		Statement stmt = conn.createStatement();
		stmt.execute("LISTEN dsupdate");
		stmt.execute("LISTEN dsdelete");
		stmt.execute("LISTEN dsinsert");
		stmt.execute("LISTEN thresholdupdate");
		stmt.execute("LISTEN updatecdedetails");
		stmt.execute("LISTEN updateblockdetails");
		stmt.execute("LISTEN cdeupdate");
//		stmt.execute("LISTEN cdemainupdate");
//		stmt.execute("LISTEN blockupdate");
//		stmt.execute("LISTEN blockdelete");
//		stmt.execute("LISTEN blockinsert");
		stmt.close();
	}

	public void run() {
		while (true) {
			try {
				// issue a dummy query to contact the backend
				// and receive any pending notifications.
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT 1");
				rs.close();
				stmt.close();

				org.postgresql.PGNotification notifications[] = pgconn.getNotifications();
				
				if (notifications != null) {
					for (int i=0; i<notifications.length; i++) {
						//System.out.println("Got notification: " + notifications[i].getName());
						if (notifications[i].getName().equals("dsupdate")){
							logger.info("EVENT: Listener found notification for datasource update");
							System.out.println("LISTENER FOUND DATASOURCE UPDATE");
							UpdateURL updateurl = new UpdateURL(conn);
						}
						else if (notifications[i].getName().equals("dsdelete")){
							logger.info("EVENT: Listener found notification for datasource delete");
							System.out.println("LISTENER FOUND DATASOURCE DELETE");
							syncXML syncxml = new syncXML(conn);
						}
						else if (notifications[i].getName().equals("dsinsert")){
							logger.info("EVENT: Listener found notification for datasource insert");
							System.out.println("LISTENER FOUND DATASOURCE INSERT");
							syncXML syncxml = new syncXML(conn);
						}
						else if (notifications[i].getName().equals("thresholdupdate")){
							logger.info("EVENT: Listener found notification for threshold update");
							System.out.println("LISTENER FOUND THRESHOLD UPDATE");
							UpdateThreshold updatethreshold = new UpdateThreshold(conn);
						}
						else if (notifications[i].getName().equals("cdeupdate")){
							logger.info("EVENT: Listener found notification for cde update");
							System.out.println("LISTENER FOUND CDE UPDATE");
							CDEDetailsUpdate cde = new CDEDetailsUpdate(conn);
						}
//						else if (notifications[i].getName().equals("cdemainupdate")){
//							logger.info("EVENT: Listener found notification for cde main update");
//							System.out.println("LISTENER FOUND CDE MAIN UPDATE");
//							CDEMainUpdate cdemain = new CDEMainUpdate(conn);
//						}
//						else if (notifications[i].getName().equals("blockupdate")){
//							logger.info("EVENT: Listener found notification for block update");
//							System.out.println("LISTENER FOUND BLOCKDETAILS UPDATE");
//							BlockChanges bc = new BlockChanges(conn);
//						}
//						else if (notifications[i].getName().equals("blockinsert")){
//							logger.info("EVENT: Listener found notification for block insert");
//							System.out.println("LISTENER FOUND BLOCKDETAILS INSERT");
//							BlockChanges bc = new BlockChanges(conn);
//						}
//						else if (notifications[i].getName().equals("blockdelete")){
//							logger.info("EVENT: Listener found notification for block delete");
//							System.out.println("LISTENER FOUND BLOCKDETAILS DELETE");
//							BlockChanges bc = new BlockChanges(conn);
//						}
					}
				}

				// wait a while before checking again for new
				// notifications
				Thread.sleep(500);
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
		}
	}

}

//class Notifier extends Thread {
//
//	private Connection conn;
//
//	public Notifier(Connection conn) {
//		this.conn = conn;
//	}
//
//	public void run() {
//		while (true) {
//			try {
//				Statement stmt = conn.createStatement();
//				stmt.execute("NOTIFY mymessage");
//				stmt.close();
//				Thread.sleep(2000);
//			} catch (SQLException sqle) {
//				sqle.printStackTrace();
//			} catch (InterruptedException ie) {
//				ie.printStackTrace();
//			}
//		}
//	}
//
//}