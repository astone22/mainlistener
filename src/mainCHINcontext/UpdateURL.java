package mainCHINcontext;

import java.io.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.sql.*;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;

import ch.qos.logback.classic.Logger;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

























import java.util.ArrayList;

























import javax.xml.parsers.*;
import javax.xml.xpath.*;

import java.io.*;

import org.postgresql.util.PSQLException;
import org.slf4j.LoggerFactory;
import org.w3c.dom.*;
import org.xml.sax.*;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * 
 * @author Alan Stone
 *
 */


public class UpdateURL {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(UpdateURL.class);
	
	public UpdateURL(Connection conn) {
		logger.info("EVENT: Starting UpdateURL.java");
		System.out.println("Starting UpdateURL.java");
		try {
	    	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
	        
	        Connection connection = conn;
			Statement statement = connection.createStatement();
			
			  //
		      //	DB STUFF - get number of sources from db, and also add them to an arraylist
		      //
		        ResultSet resultSet = null;
		        resultSet = statement.executeQuery("select count(datasource_name) from chinmain_admin_datasources;");
		        resultSet.next();
		        int numberOfSourcesFromDB = resultSet.getInt(1);
		        List<String> DBList = new ArrayList<>(numberOfSourcesFromDB);
		        List<String> URLsFromDB = new ArrayList<>(numberOfSourcesFromDB);
				resultSet = statement.executeQuery("select * from chinmain_admin_datasources;");
				while (resultSet.next()){
			        String datasourceName = resultSet.getString("datasource_name");
			      	DBList.add(datasourceName);
		        }
				resultSet = statement.executeQuery("select remote_instance_ip_addr from chinmain_admin_datasources;");
				while (resultSet.next()){
					String ip = resultSet.getString("remote_instance_ip_addr");
					URLsFromDB.add(ip);
				}
			
			//
			//	XML STUFF
			//
				// -opens xml file into document
	        File file = new File("webapps/chinapp/WEB-INF/mainCHINContext.xml");
	        String fileLocation = file.getAbsolutePath();
	        Document document = docBuilder.parse(file);
	        
	        XPathFactory xpFactory = XPathFactory.newInstance();
	        XPath xPath = xpFactory.newXPath();
	        document.getDocumentElement().normalize();
	        XPathExpression expr = xPath.compile("//beans/bean");
	        NodeList  nList = (NodeList) expr.evaluate(document, XPathConstants.NODESET); //

	        //
	        //	Grab the number of sources from XML also add them to an ArrayList
	        //
	        //
	        String xmlID = "";
	        String xmlID2 = "";
	        String xmlIDsource = "";
	        int numberOfSourcesFromXML = 0;
	        String beanID1 = "testRmtServiceHttpInvokerProxy";
	        
	        List<String> XMLList = new ArrayList<>();
	        for (int temp = 0; temp < nList.getLength(); temp++) {
	        	
	            Node nNode = nList.item(temp);
	            xmlID = ((Element) nNode).getAttribute("id");
//	            System.out.println(xmlID);
	            
	            try{
	            	xmlID2 = xmlID.substring(0, 30);
	            	xmlIDsource = xmlID.substring(30, xmlID.length());
	            }
	            catch(StringIndexOutOfBoundsException e){
	            	//e.printStackTrace();
	            }

	            if (xmlID2.equalsIgnoreCase(beanID1)){
	            	//System.out.println("match");
	            	numberOfSourcesFromXML++;
	            	XMLList.add(xmlIDsource);
	            }
	        }

    		String line = "";
    		BufferedReader in2 = null;

            String find = " <property name=\"serviceUrl\"";
            String find2 = "/chinrmtmvcsvc/testRmtService-httpinvoker\"/>";
            
            List<String> URLsFromXML = new ArrayList<>(numberOfSourcesFromXML);
    	    List<String> lines = new ArrayList<String>();
    	    in2 = new BufferedReader(new FileReader(file));
    	    line = in2.readLine();
    	    
    	    while (line != null) {
    	    	if (line.contains(find) && line.contains(find2)) {
    	    		String newline = line.substring(38, line.lastIndexOf("/chinrmtmvcsvc"));
//    	    		System.out.println("line: " + newline);
    	            URLsFromXML.add(newline);
    	            line = in2.readLine();
    	    	}
    	    	else {
	    	        lines.add(line);
	    	        line = in2.readLine();
    	    	}
    	    }
    	    
    	    in2.close();
    	    
    	    
    	    if (numberOfSourcesFromXML == numberOfSourcesFromDB) {
    		      
    	        boolean matchFound = false;
    	    	int countTemp = 0;
    	    	System.out.println("XML List: " + URLsFromXML);
    	    	System.out.println("DB List: " + URLsFromDB);
    	    	List<String> editedURLs = new ArrayList<>();
    	    	for (String i : URLsFromDB) {
    	    		for (String j : URLsFromXML) {
    	    			if (i.equalsIgnoreCase(j)) {
    	    				matchFound = true;
    	    				break;
    	    						}
    	    					}
    	    		if (!matchFound) {
    	    			editedURLs.add(i);
    	    			countTemp++;
    	    						}
    	    		matchFound = false;
    	    	}
    	        
    	    	if (editedURLs.isEmpty()){
    	    		System.out.println("No data sources have been edited from the DB");
    	    	}
    	    	else {
    	    		System.out.println("Fixing changed URLs");
	    	    		for (String a : editedURLs ){
	    	    			
		    	    		resultSet = statement.executeQuery("select datasource_name from chinmain_admin_datasources where remote_instance_ip_addr = '" + a + "';");
		    	    		resultSet.next();
		    	    		String datasourcename = resultSet.getString("datasource_name");
		    	    		ArrayList<String> datasources = new ArrayList<>();
		    	    		datasources.add(datasourcename);
		    	    		
		    	    		RemoveFromXML remove = new RemoveFromXML(fileLocation, datasourcename);
		    	    		AddToXML added = new AddToXML(connection, fileLocation, datasources);
    		    	}
    	    		System.out.println("Edited Data Source REMOTE INSTANCE IP ADDRESS: " + editedURLs);
    	    	}
    	    logger.info("EVENT: Data updated to XML file mainCHINcontext.xml");

    	    }
    	    
    } catch (SQLException e) {
        System.out.println("SQLException");
        e.printStackTrace();
    } catch (XPathExpressionException e) {
        System.out.println("XPathExpressionException");
        e.printStackTrace();
    } catch (IOException e) {
        logger.info("EVENT: FILE NOT FOUND");
    } catch (ParserConfigurationException e) {
        System.out.println("ParserConfigurationException");
        e.printStackTrace();
    } catch (SAXException e) {
        System.out.println("SAXException");
        e.printStackTrace();
    }
		
	}
	


}
