package mainCHINcontext;

import java.sql.*;

import javax.ws.rs.core.MediaType;

import org.slf4j.LoggerFactory;

import java.io.*;

import ch.qos.logback.classic.Logger;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import java.util.*;

//written by Onyi & Alan Stone

public class GetCDE {
	private static final Logger logger = (Logger) LoggerFactory.getLogger(GetCDE.class);
	public GetCDE(){
		super();
	}
	
	public void readCDE(String sourcename, Connection conn){
		try{
		Connection connection = conn;

		logger.info("EVENT: Starting GetCDE.java!");
		logger.info("DEBUG: connection: " + connection.isValid(0));
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = null;
		ClientResponse respond = null;
//		Properties prop = new Properties();
//        String userName = "", password = "", location = "";
//        prop.load(new FileInputStream("/webapps/Pathm/WEB-INF/files/data.properties"));
//        location = prop.getProperty("location");
//        userName = prop.getProperty("user");
//        password = prop.getProperty("password");
        logger.info("DEBUG: Connecting to " + sourcename);
//        Connection connection = DriverManager.getConnection(location, userName, password);
        Statement statement = connection.createStatement();
        Statement statement2 = connection.createStatement(); //need a separate statement to do execute query inside the resultset loop
        ResultSet resultSet = null;
        if(sourcename==""||sourcename==null){
        	resultSet = statement.executeQuery
            		("SELECT global_datasource_id, remote_instance_ip_addr FROM chinmain_admin_datasources "
            				+ "where is_datasource_enabled = true and is_datasource_initialized = true and is_datasource_disabled_by_remoteadmin = false;");
            //select all datasource and do this process repeatedly
        }else{
        resultSet = statement.executeQuery
        		("SELECT global_datasource_id, remote_instance_ip_addr FROM chinmain_admin_datasources "
        				+ "where datasource_short_name = \'"+ sourcename 
        				+ "\' and is_datasource_enabled = true and is_datasource_initialized = true and is_datasource_disabled_by_remoteadmin = false;");
        //select the source and do this process
        }
        System.out.println("Starting to get CDE table from remote");
        logger.info("DEBUG: Starting to get CDE table from remote");
        while(resultSet.next()){
        	String address = resultSet.getString("remote_instance_ip_addr");
        	logger.info("DEBUG: address = " + address);
        	address = address.substring(0, address.lastIndexOf("/")+1)+"cdeExtractor/main/"; //constructing the URL address for the Jersey webservice
        	logger.info("DEBUG: address = " + address);
        	System.out.println(address);
        	
        	int sourceid = resultSet.getInt("global_datasource_id");
        	logger.info("DEBUG: sourceid = " + sourceid);
        	service = client.resource(address);
//        	respond = service.path("getcde?"+sourceid).type(MediaType.TEXT_PLAIN)
//					.get(ClientResponse.class);
//        	String cdedata = respond.getEntity(String.class);
        	
        	String cdedata = service.path("getcde/"+sourceid) //the jersey webservice will return the insert statement rather than just the data.
        	.accept(MediaType.TEXT_PLAIN).get(String.class);
        	logger.info("DEBUG: cdedata = " + cdedata);
        	System.out.println(cdedata);
        	
        	try{
        	statement2.execute("DELETE FROM chinmain_admin_datasources_pi WHERE ds_id="+sourceid);
        	}
        	catch(Exception e)
        	{
//        		e.printStackTrace();
        	}
        	logger.info("EVENT: SQL Statement from CDE is: " + cdedata);
        	statement2.execute(cdedata);
        }
        
        System.out.println("GetCDE process finished");
        logger.info("EVENT: Successfully grabbed CDE data from remote and inserted into pi table on main");
        
		}
		catch(Exception e){
			e.printStackTrace();
		}

}
}
